.PHONY: all rebuild fmt

all: rebuild

rebuild: fmt
	./scripts/rebuild.sh

fmt:
	nixpkgs-fmt .

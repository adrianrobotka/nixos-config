#!/usr/bin/env nix-shell
#!nix-shell -i bash -p curl imagemagick
# shellcheck shell=bash

mkdir -p ~/img
res="1920x1080"
curl -LSs https://raw.githubusercontent.com/NixOS/nixos-artwork/master/wallpapers/nix-wallpaper-nineish-dark-gray.svg \
    | convert svg:- \
        -gamma 1.14,1.16,1.25 \
        -resize "$res^" \
        -gravity center \
        -extent "$res" \
        ~/media/pics/wallpaper.png

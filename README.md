# My NixOS config

This is my NixOS setup alongside with my dotfiles.

## Init

```
nix-shell -p yadm
yadm clone -f https://gitlab.com/adrianrobotka/nixos-config.git
```

## PC install

### Eary stage

- https://nixos.org/manual/nixos/stable/#sec-installation-manual-networking

### Later stage

```
hostnamectl hostname set dnix

nix-channel --add https://github.com/NixOS/nixos-hardware/archive/master.tar.gz nixos-hardware
nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.11.tar.gz home-manager
nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable
nix-channel --update
```

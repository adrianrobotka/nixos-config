{ pkgs, config, ... }: {
  systemd.user.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
    PATH = "$PATH:$HOME/.bin";
    SYSTEMD_PAGER = "";
  };

  xdg.userDirs = {
    enable = true;
    createDirectories = true;
    desktop = "${config.users.users.adr.home}/tabletop";
    documents = "${config.users.users.adr.home}/docs";
    download = "${config.users.users.adr.home}/internet";
    music = "${config.users.users.adr.home}/media/noise";
    pictures = "${config.users.users.adr.home}/media/pics";
    publicShare = "${config.users.users.adr.home}/tabletop/public";
    templates = "${config.users.users.adr.home}/tabletop/templates";
    videos = "${config.users.users.adr.home}/media/video";
    extraConfig = { XDG_SCREENSHOTS_DIR = "${config.users.users.adr.home}/media/screenshots"; };
  };
}

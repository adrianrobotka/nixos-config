{ config, pkgs, home, ... }: {
  dconf.settings = {
    # make virt-manager use system libvirt
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };
  };
}

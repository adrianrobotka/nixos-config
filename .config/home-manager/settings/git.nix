{ config, pkgs, home, ... }: {
  programs.git = {
    enable = true;
    userName = "Adrian Robotka";
    userEmail = "robotka.adrian@gmail.com";
    lfs.enable = true;
    signing = {
      signByDefault = true;
      key = "~/.ssh/id_ed25519";
    };
    extraConfig = {
      gpg.format = "ssh";
      core = {
        autocrlf = "input";
        pager = "bat";
      };
      pull.rebase = "true";
      push.autoSetupRemote = "true";
      init.defaultBranch = "main";
      url = {
        "ssh://git@git.sch.bme.hu/" = {
          insteadOf = "https://git.sch.bme.hu/";
        };
        "ssh://git@github.com/" = { insteadOf = "https://github.com/"; };
      };
    };
    includes = [{
      path = (pkgs.formats.ini { }).generate "axoflow.gitconfig" {
        user.email = "adrian.robotka@axoflow.com";
      };
      condition = "gitdir:~/axo/**";
    }];
  };
}

{ config, pkgs, home, ... }: {
  imports = [ ./libvirt.nix ./git.nix ./zsh.nix ./nvim.nix ];
}

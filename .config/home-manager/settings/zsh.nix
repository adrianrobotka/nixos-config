{ pkgs, lib, ... }:
let
  homeDir = builtins.getEnv "HOME";
  kubeFolder = builtins.toPath "${homeDir}/.kube";
  filesInKube = builtins.readDir kubeFolder;
  kubeYamls = builtins.filter (f: lib.hasSuffix ".yaml" "${f}")
    (builtins.attrNames filesInKube);
  kubeYamlsFullPath = map (f: "${kubeFolder}/${f}") kubeYamls;
  mainKubeConfig = "${kubeFolder}/config";
  kubeConfigs = [ mainKubeConfig ] ++ kubeYamlsFullPath;
  kubeconfig = builtins.concatStringsSep ":" kubeConfigs;
in {
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    enableAutosuggestions = true;
    syntaxHighlighting.enable = true;
    enableCompletion = true;
    enableVteIntegration = true;
    historySubstringSearch = { enable = true; };
    oh-my-zsh = {
      enable = true;
      plugins = [
        "git"
        "aliases"
        "colored-man-pages"
        "command-not-found"
        "golang"
        "tmux"
        # "ansible"
        "kubectl"
        "helm"
        "terraform"
        "terraform"
        "taskwarrior"
        "systemd"
      ];
      theme = "";
    };
    shellAliases = {
      c = "code .";
      e = "$EDITOR";
      i = "grep -i";
      o = "xdg-open";
      p = "python";
      s = "git status";
      w = "which";

      tl = "task list";
      td = "task delete";
      ta = "task add";

      ssh = "TERM=xterm-256color ssh";
      cat = "bat -p";
      less = "less -r";
      pkg = ''
        NIXPKGS_ALLOW_UNFREE=1 nix-shell --run "env SHELL='$(which zsh)' zsh" -p'';
      gcloud = "TERM=xterm-256color gcloud";
    };
    plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
      {
        name = "powerlevel10k-config";
        src = lib.cleanSource ../files;
        file = "p10k.zsh";
      }
    ];
  };

  programs.mcfly = {
    enable = true;
    enableZshIntegration = true;
    # enableLightTheme = true;
    fuzzySearchFactor = 2;
  };

  home.sessionVariables = {
    KUBECONFIG = kubeconfig;
    MCFLY_RESULTS = "50";
  };
}

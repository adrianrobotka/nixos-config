{ config, pkgs, home, ... }:

{
  home.packages = with pkgs; [
    # Basic tools
    curl
    jq
    yq-go
    file
    zip
    unzip
    tree
    lsof

    # CLI ++
    git
    git-lfs
    yadm
    tmux
    openssh
    dig
    nmap
    unixtools.xxd

    # CLI work tools
    terraform
    (google-cloud-sdk.withExtraComponents
      ([ google-cloud-sdk.components.gke-gcloud-auth-plugin ]))
    kubectl
    kubernetes-helm

    # CLI desktop env
    mcfly
    bat
    taskwarrior

    # dev land
    go
    gcc
    golangci-lint
    
    gnumake
    clang-tools
    
    nixpkgs-fmt
    (python3.withPackages (ps: with ps; [ autopep8 ]))
    buf # protobuf

    # CLI nice to haves
    ddcutil # monitor brightness
    pwgen
  ];

  nixpkgs.config.allowUnfree = true;
}

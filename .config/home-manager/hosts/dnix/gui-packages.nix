{ pkgs, ... }:
let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in {
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    audacity
    obs-studio
    mediainfo

    # messaging
    unstable.beeper

    # jetbrains.goland
    # jetbrains.webstorm
    # jetbrains.pycharm-professional
    # jetbrains.clion
  ];
}

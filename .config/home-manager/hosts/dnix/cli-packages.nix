{ config, pkgs, home, ... }:

{
  home.packages = with pkgs; [
    # jdk8

    # CLI nice to haves
    ddcutil # monitor brightness
    neofetch
    pwgen
    nix-index

    # fun stuff
    cowsay
    figlet
    cmatrix
  ];

  nixpkgs.config.allowUnfree = true;
}

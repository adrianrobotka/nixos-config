{ config, pkgs, lib, home, ... }:

let
  hostNameRaw = builtins.readFile /etc/hostname;
  hostName = lib.strings.removeSuffix "\n" hostNameRaw;
in {
  home.username = builtins.getEnv "USER";
  home.homeDirectory = builtins.getEnv "HOME";

  home.stateVersion = "23.11";

  imports = [
    ./cli-packages.nix
    ./gui-packages.nix
    ./sway.nix
    ./settings
    ./services.nix
    ./hosts/${hostName}
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

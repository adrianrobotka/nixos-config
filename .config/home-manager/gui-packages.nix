{ pkgs, ... }:
let
  # add using `nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable`
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in {
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    # Desktop Environment
    rofimoji
    sway-contrib.grimshot
    pavucontrol
    playerctl

    # The ususal
    firefox-bin
    vlc
    slack
    unstable.telegram-desktop
    spotify

    font-awesome
    fira-code-nerdfont
    (pkgs.nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })

    vscode
    meld
    openlens
    qpwgraph # pipewire graph viewer

    # Nice to haves
    remmina
    gimp
    inkscape
  ];
}

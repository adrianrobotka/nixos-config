{ config, pkgs, home, lib, ... }: {
  services.mpris-proxy.enable = true;
  services.easyeffects.enable = true;

  services.syncthing = { enable = true; };

  home.sessionVariablesExtra = ''
    if [[ -z "$SSH_AUTH_SOCK" ]]; then
      export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ssh-agent
    fi
  '';

  systemd.user.services.ssh-agent = {
    Install.WantedBy = [ "default.target" ];

    Unit = {
      Description = "SSH authentication agent";
      Documentation = "man:ssh-agent(1)";
    };

    Service = {
      ExecStart = "${pkgs.openssh}/bin/ssh-agent -D -a %t/ssh-agent";
    };
  };

  services.syncthing.tray.enable = true;

  # systemd.user.targets.tray = {
  #   Unit = {
  #     Description = "Home Manager System Tray";
  #     Requires = [ "plasma-workspace.target" ]/
  #   };
  # };

  systemd.user.services.clean-downloads = let
    cleaner-script = pkgs.writeText "clean-downloads.sh" ''
      #!/bin/sh

      set -eo pipefail

      find ~/Downloads -type f -mtime +30 -delete
      find ~/Downloads -type d -empty -delete
    '';
  in {
    Unit = { Description = "Clean Downloads"; };

    Service = { ExecStart = "sh ${cleaner-script}"; };
  };

  systemd.user.timers.clean-downloads = {
    Unit.Description = "Clean Downloads";
    Install.WantedBy = [ "timers.target" ];
    Timer = {
      OnCalendar = "weekly";
      Persistent = true;
    };
  };
}

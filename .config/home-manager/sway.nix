{ config, pkgs, lib, ... }:

{
  wayland.windowManager.sway = {
    enable = true;
    systemd.enable = true;

    config = rec {
      modifier = "Mod4";
      terminal = "foot";

      fonts = {
        names = [ "Fira Code" ];
        size = 10.5;
      };
      bars = [ ]; # waybar will be configured later
      input = {
        "*" = { xkb_numlock = "enable"; };
        "type:touchpad" = {
          tap = "enabled";
          scroll_factor = "0.4";
        };
        "type:keyboard" = {
          xkb_layout = "us,hu";
          # https://man.archlinux.org/man/xkeyboard-config.7#Switching_to_another_layout
          xkb_options = "grp:caps_toggle";
        };
      };
      keybindings = {
        "${modifier}+Return" = "exec ${terminal}";
        "${modifier}+Shift+q" = "kill";
        "${modifier}+d" = "exec ${pkgs.rofi}/bin/rofi -show drun";

        "${modifier}+h" = "focus left";
        "${modifier}+j" = "focus down";
        "${modifier}+k" = "focus up";
        "${modifier}+l" = "focus right";

        # "${modifier}+Left" = "focus left";
        # "${modifier}+Down" = "focus down";
        # "${modifier}+Up" = "focus up";
        # "${modifier}+Right" = "focus right";

        "${modifier}+Shift+h" = "move left";
        "${modifier}+Shift+j" = "move down";
        "${modifier}+Shift+k" = "move up";
        "${modifier}+Shift+l" = "move right";

        "${modifier}+Shift+Left" = "move left";
        "${modifier}+Shift+Down" = "move down";
        "${modifier}+Shift+Up" = "move up";
        "${modifier}+Shift+Right" = "move right";

        "${modifier}+Control+h" = "move workspace to output left";
        "${modifier}+Control+j" = "move workspace to output down";
        "${modifier}+Control+k" = "move workspace to output up";
        "${modifier}+Control+l" = "move workspace to output right";

        "${modifier}+b" = "splith";
        "${modifier}+v" = "splitv";
        "${modifier}+f" = "fullscreen toggle";
        "${modifier}+a" = "focus parent";
        "${modifier}+i" = "focus parent";

        # "${modifier}+s" = "layout stacking";
        "${modifier}+w" = "layout tabbed";
        "${modifier}+e" = "layout toggle split";

        "${modifier}+Shift+space" = "floating toggle";
        "${modifier}+space" = "focus mode_toggle";

        "${modifier}+1" = "workspace 1";
        "${modifier}+2" = "workspace 2";
        "${modifier}+3" = "workspace 3";
        "${modifier}+4" = "workspace 4";
        "${modifier}+5" = "workspace 5";
        "${modifier}+6" = "workspace 6";
        "${modifier}+7" = "workspace 7";
        "${modifier}+8" = "workspace 8";
        "${modifier}+9" = "workspace 9";
        "${modifier}+0" = "workspace 10";
        "${modifier}+F1" = "workspace 11";
        "${modifier}+F2" = "workspace 12";
        "${modifier}+F3" = "workspace 13";
        "${modifier}+F4" = "workspace 14";
        "${modifier}+F5" = "workspace 15";
        "${modifier}+F6" = "workspace 16";
        "${modifier}+F7" = "workspace 17";
        "${modifier}+F8" = "workspace 18";
        "${modifier}+F9" = "workspace 19";
        "${modifier}+F10" = "workspace 20";
        "${modifier}+F11" = "workspace 21";
        "${modifier}+F12" = "workspace 22";

        "${modifier}+Shift+1" = "move container to workspace 1";
        "${modifier}+Shift+2" = "move container to workspace 2";
        "${modifier}+Shift+3" = "move container to workspace 3";
        "${modifier}+Shift+4" = "move container to workspace 4";
        "${modifier}+Shift+5" = "move container to workspace 5";
        "${modifier}+Shift+6" = "move container to workspace 6";
        "${modifier}+Shift+7" = "move container to workspace 7";
        "${modifier}+Shift+8" = "move container to workspace 8";
        "${modifier}+Shift+9" = "move container to workspace 9";
        "${modifier}+Shift+0" = "move container to workspace 10";
        "${modifier}+Shift+F1" = "move container to workspace 11";
        "${modifier}+Shift+F2" = "move container to workspace 12";
        "${modifier}+Shift+F3" = "move container to workspace 13";
        "${modifier}+Shift+F4" = "move container to workspace 14";
        "${modifier}+Shift+F5" = "move container to workspace 15";
        "${modifier}+Shift+F6" = "move container to workspace 16";
        "${modifier}+Shift+F7" = "move container to workspace 17";
        "${modifier}+Shift+F8" = "move container to workspace 18";
        "${modifier}+Shift+F9" = "move container to workspace 19";
        "${modifier}+Shift+F10" = "move container to workspace 20";
        "${modifier}+Shift+F11" = "move container to workspace 21";
        "${modifier}+Shift+F12" = "move container to workspace 22";

        "${modifier}+Shift+minus" = "move scratchpad";
        "${modifier}+minus" = "scratchpad show";

        "${modifier}+Shift+c" = "reload";
        "${modifier}+Shift+e" =
          "exec swaynag -t warning -m 'Exit?' -b 'Yes, exit sway' 'swaymsg exit'";

        "${modifier}+r" = "mode resize";

        "${modifier}+Escape" = "exec loginctl lock-session";
        "${modifier}+Shift+Escape" = "exec systemctl suspend";
        "${modifier}+Shift+d" =
          "exec ${pkgs.rofimoji}/bin/rofimoji --action copy";
        "${modifier}+n" = "exec ${pkgs.mako}/bin/makoctl dismiss -a";

        "Print" = "exec ${pkgs.sway-contrib.grimshot}/bin/grimshot copy area";
        "Shift+Print" =
          "exec ${pkgs.sway-contrib.grimshot}/bin/grimshot save area";

        "--locked XF86MonBrightnessDown" =
          "exec ${pkgs.brightnessctl}/bin/brightnessctl set 5%-";
        "--locked XF86MonBrightnessUp" =
          "exec ${pkgs.brightnessctl}/bin/brightnessctl set 5%+";

        "--locked XF86AudioMute" =
          "exec wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle";
        "--locked XF86AudioLowerVolume" =
          "exec wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-";
        "--locked XF86AudioRaiseVolume" =
          "exec wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+";
        "--locked XF86AudioMicMute" =
          "exec wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle";

        "--locked XF86AudioPlay" =
          "exec ${pkgs.playerctl}/bin/playerctl play-pause";
        "--locked XF86AudioPause" =
          "exec ${pkgs.playerctl}/bin/playerctl pause";
        "--locked XF86AudioNext" = "exec ${pkgs.playerctl}/bin/playerctl next";
        "--locked XF86AudioPrev" =
          "exec ${pkgs.playerctl}/bin/playerctl previous";

        "--locked ${modifier}+Down" =
          "exec ${pkgs.playerctl}/bin/playerctl play-pause";
        "--locked ${modifier}+End" =
          "exec ${pkgs.playerctl}/bin/playerctl next";
      };
      output = { "*" = { bg = "~/media/pics/wallpaper.png fill"; }; };
    };
  };

  services = {
    swayidle = let
      swaylock =
        "${pkgs.swaylock}/bin/swaylock --image ~/media/pics/wallpaper.png";
      swaymsg = "${pkgs.sway}/bin/swaymsg";
      pgrep = "${pkgs.procps}/bin/pgrep";
      loginctl = "${pkgs.systemd}/bin/loginctl";
    in {
      enable = true;
      systemdTarget = "sway-session.target";
      events = [
        {
          event = "lock";
          command = "${swaylock}";
        }
        {
          event = "before-sleep";
          command = "${loginctl} lock-session";
        }
      ];
      timeouts = let
        lock_time = 300;
        screen_sleep = 300;
      in [
        {
          timeout = lock_time;
          command = "${loginctl} lock-session";
        }
        {
          timeout = lock_time + screen_sleep;
          command = ''${swaymsg} "output * power off"'';
          resumeCommand = ''${swaymsg} "output * power on"'';
        }
        # {
        #   timeout = screen_sleep;
        #   command = ''if ${pgrep} swaylock; then ${swaymsg} "output * power off"; fi'';
        #   resumeCommand = ''if ${pgrep} swaylock; then ${swaymsg} "output * power on"; fi'';
        # }
      ];
      # extraArgs = [ "-d" ];
    };
    mako.enable = true;
    gnome-keyring = {
      enable = true;
      components = [ "pkcs11" "ssh" "secrets" ];
    };
  };
  programs.waybar = {
    enable = true;
    systemd.enable = true;
  };

  systemd.user.services = let
    kanshiConfig = pkgs.writeText "config" ''
      # swaymsg -t get_outputs

      profile docked {
      	output eDP-1 disable
      	output "Lenovo Group Limited LEN T22i-10 V5MR6133" enable position 0,0     mode 1920x1080
      	output "Lenovo Group Limited LEN T22i-10 V5MR6118" enable position 1920,0  mode 1920x1080
      	output "Lenovo Group Limited LEN T22i-10 V5MR6128" enable position 3840,0  mode 1920x1080
        exec /home/adr/.bin/fix_docked
      }
      # profile pp {
      # 	output HDMI-A-1 enable position 0,0 mode 1920x1080
      # 	output eDP-1 enable position 0,1080 mode 1920x1080
      # }
      profile solo {
        output eDP-1 enable
      }
    '';
  in {
    kanshi = {
      Unit = { Description = "kanshi monitor setups"; };
      Service = {
        Type = "simple";
        ExecStart = "${pkgs.kanshi}/bin/kanshi -c ${kanshiConfig}";
        Restart = "on-failure";
      };
      Install = { WantedBy = [ "sway-session.target" ]; };
    };
    nm-applet = {
      Unit = { Description = "Network Manager Applet"; };
      Service = {
        ExecStart = "${pkgs.networkmanagerapplet}/bin/nm-applet --indicator";
        Restart = "on-abort";
      };
      Install = { WantedBy = [ "sway-session.target" ]; };
    };
    blueman-applet = {
      Unit = { Description = "Blueman Applet"; };
      Service = {
        ExecStart = "${pkgs.blueman}/bin/blueman-applet";
        Restart = "on-abort";
      };
      Install = { WantedBy = [ "sway-session.target" ]; };
    };
  };
}

{ config, pkgs, ... }: {
  services.xserver.displayManager.sessionPackages = [ pkgs.sway ];

  environment.sessionVariables = { NIXOS_OZONE_WL = "1"; };

  # enabling Sway as a session in GDM
  # configured with home-manager
  security.polkit.enable = true;

  security.pam.services.swaylock.text = ''
    # PAM configuration file for the swaylock screen locker. By default, it includes
    # the 'login' configuration file (see /etc/pam.d/login)
    auth include login
  '';

  # xdg.portal = {
  #   enable = true;
  #   xdgOpenUsePortal = true;
  #   extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  # };

  # Fix GTK on non-Gnome Wayland
  programs.dconf.enable = true;
  environment.systemPackages = with pkgs; [
    gnome.gnome-settings-daemon
    gsettings-desktop-schemas
  ];
}

# Help:
# - configuration.nix(5) man page
# - NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

let
  hostNameRaw = builtins.readFile /etc/hostname;
  hostName = lib.strings.removeSuffix "\n" hostNameRaw;
in {
  imports = [
    /etc/nixos/hardware-configuration.nix
    ./sway.nix
    ./hosts/${hostName}.nix
  ];

  environment.sessionVariables = {
    NIXOS_CONFIG =
      "${config.users.users.adr.home}/.config/nixos/configuration.nix";
  };

  ### Boot ###

  boot.loader.systemd-boot = {
    enable = true;
    configurationLimit = 4;
  };
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelParams = [
    # Silences boot messages
    "quiet"
    # Silences successfull systemd messages from the initrd
    "rd.systemd.show_status=false"
    # If booting fails drop us into a shell where we can investigate
    "boot.shell_on_fail"
    # Show a splash screen
    "splash"
  ];

  # Enable tpm2 boot time to unlock LUKS
  boot.initrd.systemd.enable = true;

  boot.plymouth = let theme = "cuts_alt";
  in {
    enable = true;
    inherit theme;
    font = "${pkgs.ibm-plex}/share/fonts/opentype/IBMPlexSans-Text.otf";
    themePackages = [
      (pkgs.adi1090x-plymouth-themes.override { selected_themes = [ theme ]; })
    ];
  };

  boot.kernel.sysctl = { "kernel.sysrq" = 1; };

  # TODO review this
  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  ### Networking ###

  networking.networkmanager.enable = true;
  networking.nftables.enable = true;

  # networking.firewall = {
  #   enable = true;
  #   allowedTCPPorts = [ 8080 22000 ];
  #   allowedUDPPorts = [ 22000 67 68 ];
  #   trustedInterfaces = [ "nonexistant0" "virbr0" ];
  # };

  systemd.services.NetworkManager-wait-online.enable = false;
  services.openssh.enable = true;

  ### Locale ###

  time.timeZone = "Europe/Budapest";

  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "hu_HU.UTF-8";
    LC_IDENTIFICATION = "hu_HU.UTF-8";
    LC_MEASUREMENT = "hu_HU.UTF-8";
    LC_MONETARY = "hu_HU.UTF-8";
    LC_NAME = "hu_HU.UTF-8";
    LC_NUMERIC = "hu_HU.UTF-8";
    LC_PAPER = "hu_HU.UTF-8";
    LC_TELEPHONE = "hu_HU.UTF-8";
    LC_TIME = "hu_HU.UTF-8";
  };

  ### X11 ###

  services.xserver = {
    enable = true;

    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;

    # Configure keymap in X11
    layout = "us";
    xkbVariant = "";
  };

  services.printing.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    wireplumber.enable = true;
  };
  hardware.bluetooth = {
    enable = true;
    settings = { General = { Experimental = true; }; };
  };

  ### Users ###

  users.users.adr = {
    isNormalUser = true;
    description = "Adrian Robotka";
    extraGroups = [
      "networkmanager"
      "wheel"
      "libvirtd"
      "input"
      "wireshark"
      "adbusers"
      "rtkit"
      "dialout"
      "tss"
    ];
  };

  ### Packages ###

  nixpkgs.config.allowUnfree = true;

  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;

  nix = {
    gc = {
      automatic = true;
      options = "--delete-older-than 14d";
    };
  };

  environment.systemPackages = with pkgs; [
    zsh
    alacritty
    foot

    sway
    swayidle
    swaylock
    swaybg
    rofi

    wl-clipboard
    brightnessctl
    wdisplays

    git
    curl
    jq
    htop
    sysstat

    sbctl
    tpm2-tools
    tpm2-tss
  ];

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  environment.shells = with pkgs; [ zsh ];

  services.flatpak.enable = true;

  virtualisation = {
    podman = {
      enable = true;
      defaultNetwork.settings = { dns_enabled = true; };
    };
    docker.enable = true;
  };

  ### TPM ###

  security.tpm2.enable = true;
  security.tpm2.pkcs11.enable = true;
  security.tpm2.tctiEnvironment.enable = true;

  ### Hacks ###

  # Make /bin/bash available for non-Nix-aware programs
  system.activationScripts.binbash =
    "rm -f /bin/bash; ln -sf '${pkgs.bash}/bin/bash' /bin/bash";

  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
